var User = require('../models/users');

var config = require('../../config');

var secretKey = config.secretKey;

var jsonwebtoken = require('jsonwebtoken'); 

function createToken(user) {
    var token = jsonwebtoken.sign({
        _id: user._id,
        name: user.name,
        username: user.username
    }, secretKey, {
        expiresIn: 60*60*24
    });
    return token;
}

module.exports = function(app, express){
    var api = express.Router();

    api.use(function(req, res, next){
        console.log('Someone is dealing with our app');
        next();
    })

    api.post('/signup', function(req, res){
        var user = new User({
            name:  req.body.name,
            username: req.body.username,
            password: req.body.password,
            role: req.body.role
        });

        user.save(function(err){
            if(err){
                res.send(err);
                return;
            }
            res.json({message: 'User has been created', success:true});
        });
    })

        .get('/users',function (req, res) {
            User.find({}, function (err, users) {
                if(err){
                    res.send(err);
                    return;
                }
                res.json(users);
            });
        })

        .post('/login', function (req, res) {
            User.findOne({
                username: req.body.username
            }).select('password').exec(function (err, user) {
                if(err) throw err;
                if(!user){
                    res.send({message:' User does not exist'})
                } else if(user){
                    var validPassword = user.comparePassword(req.body.password);

                    if(!validPassword){
                        res.send({message: "Invalid Password"});
                    } else {
                        // then create a token

                        var token = createToken(user);
                        res.json({
                            sucess:true,
                            message : "Successfully Loggedin!",
                            token: token
                        });
                    }
                }
            });
        });

        api.use(function (req, res, next) {
        console.log("Somebody just come to our app!");

        var token = req.body.token || req.param('token') ||  req.headers['x-access-token']

        if(token){
            jsonwebtoken.verify(token, secretKey, function (err, decoded) {
                if(err){
                    res.status(403).send({success: false, message:'Failed to authenticate user'});
                } else{
                    req.decoded = decoded;
                    next();
                }
            });
        } else{
                res.status(403).send({success: false, message: 'no Token provided'});
        }
    });


        api.get('/', function(req, res){
            res.json("hello world");
        });

    return api;
}