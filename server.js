var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');


var config = require('./config');

mongoose.connect(config.db ,function (err) {
    if(err)
    {
        console.log(err);
    } else{
        console.log('connected to Mongolab!!');
    }
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var api = require('./app/routes/api')(app,express);
app.use('/api',api);

app.use(express.static(__dirname + '/public'));

app.listen(config.PORT);
console.log('Magic happens on port ' + config.PORT);
